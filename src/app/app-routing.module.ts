import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './core/guards/login.guard';

import { ComponentLayoutComponent } from './modulos/component-layout/component-layout.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },

  {
    path: 'login',
    loadChildren: () => import('./modulos/modulo-login/modulo-login.module').then(m => m.ModuloLoginModule)
  },

  {
    path: '',
    component: ComponentLayoutComponent,
    canActivate: [LoginGuard],
    loadChildren: () => import('./modulos/modulos.module').then(m => m.ModulosModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
