import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UsuarioService } from '../services/http/usuario/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(
    private _usuarioService: UsuarioService,
    private router: Router
  ) { }

  canActivate() {
    if (this._usuarioService.estaLogeado()) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }





}
