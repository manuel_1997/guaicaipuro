import { Component } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
  animations: [
    trigger('overlay', [
      state('open', style({
        opacity: 1,
      })),
      state('closed', style({
        transform: 'scale3d(.0, .0, .0)'
      })),
      transition('open => closed', style({ opacity: .5 })),
      transition('closed => open', style({ opacity: 0 })),
    ]),
    trigger('modal', [
      state('open', style({
        transform: 'scale3d(1, 1, 1)'
      })),
      state('closed', style({
        transform: 'scale3d(.0, .0, .0)'
      })),
      transition('open => closed', animate(160)),
      transition('closed => open', animate(160)),
    ])
  ]
})
export class ModalComponent {
  public show: boolean = false;

  modalToggle() {
    this.show = !this.show;
  }

}

// [@modal]="!show ? 'closed' : 'open'"

// animations: [
//   trigger('modal', [
//     state('closed', style({
//       transform: 'scaleY(0)'
//     })),
//     state('open', style({
//       transform: 'scaleY(1)'
//     })),
//     transition('closed => open', animate('200ms ease-in')),
//     transition('open => closed', animate('200ms ease-out'))
//   ])
// ]

// animations: [
//   trigger('modal', [
//     state('open', style({
//       transform: 'scale3d(.3, .3, .3)'
//     })),
//     state('closed', style({
//       transform: 'scale3d(.0, .0, .0)'
//     })),
//     transition('open => closed', [
//       animate(150)
//     ]),
//     transition('closed => open', [
//       animate(150)
//     ]),
//   ])
// ]


// animations: [
//   trigger('modal', [
//     transition('void => *', [
//       style({ transform: 'scale3d(.3, .3, .3)' }),
//       animate(150)
//     ]),
//     transition('* => void', [
//       animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
//     ])
//   ])
// ]