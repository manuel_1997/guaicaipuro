import { AfterViewInit, Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/core/services/http/usuario/usuario.service'

declare function init_animation(): any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
  ]
})
export class SidebarComponent implements AfterViewInit {

  constructor(public _usuarioService: UsuarioService) { }

  ngAfterViewInit(): void {
    init_animation();
  }



}
