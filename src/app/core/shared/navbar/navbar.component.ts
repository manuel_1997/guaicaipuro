import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/http/usuario/usuario.service'

declare function init_animation(): any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor(private _usuarioService: UsuarioService) { }

  usuario!: string;
  foto!: string;

  ngOnInit(): void {
    init_animation()
    this.obtenerUsuario();
  }

  obtenerUsuario() {
    this.usuario = this._usuarioService.usuario.per_nombres + ' ' + this._usuarioService.usuario.per_apellidos;
    this.foto = this._usuarioService.usuario.Foto;
    if (this.foto != null) {
      this.foto = 'https://repositorio.unellez.edu.ve/terepaima/fotoperf/thumbs/' + this.foto;
    } else {
      this.foto = 'assets/img/profile.jpg';
    }
  }

  salir() {
    this._usuarioService.logout();
  }

}
