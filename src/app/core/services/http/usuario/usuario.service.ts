import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient, private router: Router) {
    this.cargarStorage();
  }

  api = environment.url_api;
  usuario: any;
  token!: string;
  menu: any[] = [];

  estaLogeado(): Boolean {
    if (localStorage.getItem('token')) {
      return true
    } else {
      return false
    }
  }

  login(admin: any) {
    return this.http.post<any>(this.api + 'login', admin)
      .pipe(map((res: any) => {
        this.guardarStorage(res.token, res.usuario, res.menu)
        this.router.navigate(['/home'])
        return true;
      }),
        catchError(err => {
          alert(err.error.message);
          return throwError(err)
        })
      );
  }

  guardarStorage(token: string, usuario: any, menu: any) {
    localStorage.setItem('token', token)
    localStorage.setItem('usuario', JSON.stringify(usuario))
    localStorage.setItem('menu', JSON.stringify(menu))

    this.token = token;
    this.usuario = usuario;
    this.menu = menu;
  }

  cargarStorage() {
    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token')!;
      this.usuario = JSON.parse(localStorage.getItem('usuario')!);
      this.menu = JSON.parse(localStorage.getItem('menu')!);
    } else {
      this.token = '';
      this.usuario = null;
      this.menu = [];
    }
  }

  logout() {
    this.token = '';
    this.usuario = null;
    this.menu = [];

    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    localStorage.removeItem('menu');
    this.router.navigate(['/login']);
  }


}
