import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModulosRoutingModule } from './modulos-routing.module';
import { SharedModule } from '../core/shared/shared.module';
import { ComponentLayoutComponent } from './component-layout/component-layout.component';


@NgModule({
  declarations: [
    ComponentLayoutComponent,
  ],
  imports: [
    CommonModule,
    ModulosRoutingModule,
    SharedModule
  ]
})
export class ModulosModule { }
