import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EnviarComponent } from './enviar/enviar.component';
import { EnviadasComponent } from './enviadas/enviadas.component';
import { AprobacionComponent } from './aprobacion/aprobacion.component';

const routes: Routes = [
  { path: 'enviar', component: EnviarComponent },
  { path: 'enviadas', component: EnviadasComponent },
  { path: 'aprobar', component: AprobacionComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuloCircularesRoutingModule { }
