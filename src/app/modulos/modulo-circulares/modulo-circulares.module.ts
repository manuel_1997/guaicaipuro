import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuloCircularesRoutingModule } from './modulo-circulares-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { EnviarComponent } from './enviar/enviar.component';
import { EnviadasComponent } from './enviadas/enviadas.component';
import { AprobacionComponent } from './aprobacion/aprobacion.component';


@NgModule({
  declarations: [
    EnviarComponent,
    EnviadasComponent,
    AprobacionComponent
  ],
  imports: [
    CommonModule,
    ModuloCircularesRoutingModule,
    ReactiveFormsModule
  ]
})
export class ModuloCircularesModule { }
