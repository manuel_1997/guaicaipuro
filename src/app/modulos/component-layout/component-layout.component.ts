import { Component, OnInit } from '@angular/core';

declare function init_animation(): any;

@Component({
  selector: 'app-component-layout',
  templateUrl: './component-layout.component.html',
  styles: [
  ]
})
export class ComponentLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    init_animation();
  }

}
