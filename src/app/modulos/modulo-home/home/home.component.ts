import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/core/services/http/usuario/usuario.service';
import { ModalComponent } from 'src/app/core/components/modal/modal.component'


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  constructor(private _usuarioService: UsuarioService) { }

  usuario!: string;

  ngOnInit(): void {
    this.obtenerUsuario();
  }

  obtenerUsuario() {
    this.usuario = this._usuarioService.usuario.per_nombres + ' ' + this._usuarioService.usuario.per_apellidos;
  }

}
