import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuloHomeRoutingModule } from './modulo-home-routing.module';
import { ModalModule } from 'src/app/core/components/modal/modal.module';

import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    ModuloHomeRoutingModule,
    ModalModule
  ]
})
export class ModuloHomeModule { }
