import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http/";
import { ReactiveFormsModule } from '@angular/forms';

import { ModuloLoginRoutingModule } from './modulo-login-routing.module';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    ModuloLoginRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class ModuloLoginModule { }
