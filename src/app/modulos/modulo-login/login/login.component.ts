import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/core/services/http/usuario/usuario.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor(private _usuarioService: UsuarioService) { }

  formLogin = new FormGroup({
    usuario: new FormControl('', Validators.required),
    contrasena: new FormControl('', Validators.required),
  });

  submit: boolean = false;

  ngOnInit(): void {
  }

  enivarForm() {
    this.submit = true;
    if (this.formLogin.valid) {
      this._usuarioService.login(this.formLogin.value)
        .subscribe();
    }
  }

}
