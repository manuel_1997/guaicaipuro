import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RecibidasComponent } from './recibidas/recibidas.component';


const routes: Routes = [
  { path: 'recibidas', component: RecibidasComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuloAprobacionesRoutingModule { }
