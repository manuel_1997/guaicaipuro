import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModuloAprobacionesRoutingModule } from './modulo-aprobaciones-routing.module';
import { RecibidasComponent } from './recibidas/recibidas.component';


@NgModule({
  declarations: [
    RecibidasComponent
  ],
  imports: [
    CommonModule,
    ModuloAprobacionesRoutingModule
  ]
})
export class ModuloAprobacionesModule { }
