import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./modulo-home/modulo-home.module').then(m => m.ModuloHomeModule)
  },

  {
    path: 'aprobaciones',
    loadChildren: () => import('./modulo-aprobaciones/modulo-aprobaciones.module').then(m => m.ModuloAprobacionesModule)
  },

  {
    path: 'solicitudes',
    loadChildren: () => import('./modulo-solicitudes/modulo-solicitudes.module').then(m => m.ModuloSolicitudesModule)
  },

  {
    path: 'ordenes',
    loadChildren: () => import('./modulo-ordenes/modulo-ordenes.module').then(m => m.ModuloOrdenesModule)
  },

  {
    path: 'circulares',
    loadChildren: () => import('./modulo-circulares/modulo-circulares.module').then(m => m.ModuloCircularesModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulosRoutingModule { }
