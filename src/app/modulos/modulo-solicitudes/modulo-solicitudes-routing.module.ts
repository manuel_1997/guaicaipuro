import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SeleccionarComponent } from './seleccionar/seleccionar.component';
import { EnviarComponent } from './enviar/enviar.component';
import { EnviadasComponent } from './enviadas/enviadas.component';
import { RecibidasComponent } from './recibidas/recibidas.component';

const routes: Routes = [
  { path: '', redirectTo: 'seleccionar', pathMatch: 'full' },
  { path: 'seleccionar', component: SeleccionarComponent },
  { path: 'enviar/:id', component: EnviarComponent },
  { path: 'enviadas', component: EnviadasComponent },
  { path: 'recibidas', component: RecibidasComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuloSolicitudesRoutingModule { }
