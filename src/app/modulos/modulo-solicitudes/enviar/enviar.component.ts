import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
//import { ModalService } from "src/app/services/modal.service";


@Component({
  selector: 'app-enviar',
  templateUrl: './enviar.component.html',
  styles: [
  ]
})
export class EnviarComponent implements OnInit {

  constructor(
    private activateRouter: ActivatedRoute,
    private fb: FormBuilder,
    // public _modalService: ModalService
  ) { }

  form!: FormGroup;
  tipoForm!: number;

  ngOnInit(): void {
    this.activateRouter.params.subscribe(params => {
      this.tipoForm = params['id'];
      this.tipoSolicitud();
    })
  }

  tipoSolicitud() {

    if (this.tipoForm == 1) {
      this.form = this.fb.group({
        cedula: ['', Validators.required],
        cargo_propuesto: [],
        descripcion: [],
      })
    } else if (this.tipoForm == 2) {
      this.form = this.fb.group({
        cedula: ['', Validators.required],
        archivo: [],
        descripcion: [],
      })
    } else if (this.tipoForm == 3 || this.tipoForm == 4 || this.tipoForm == 5 || this.tipoForm == 6) {
      this.form = this.fb.group({
        cedula: ['', Validators.required],
        cargo_propuesto: ['', Validators.required],
        descripcion: [],
      })
    } else if (this.tipoForm == 7) {
      this.form = this.fb.group({
        cedula: ['', Validators.required],
        cargo_propuesto: ['', Validators.required],
        archivo: ['', Validators.required],
        descripcion: [],
      })
    } else if (this.tipoForm == 8) {
      this.form = this.fb.group({
        cedula: ['', Validators.required],
        nombre: ['', Validators.required],
        apellido: ['', Validators.required],
        cargo_propuesto: ['', Validators.required],
        sintesis: ['', Validators.required],
        copia_cedula: ['', Validators.required],
        rif: ['', Validators.required],
        banco: ['', Validators.required],
      })
    } else if (this.tipoForm == 9) {
      this.form = this.fb.group({
        cedula: ['', Validators.required],
        archivo: [],
        descripcion: ['', Validators.required],
      })
    }

  }

  abrirModal() {
    //this._modalService.abrirModal(this.form.value);
  }

  enviarForm() {

  }

}

