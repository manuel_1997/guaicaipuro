import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuloSolicitudesRoutingModule } from './modulo-solicitudes-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { SeleccionarComponent } from './seleccionar/seleccionar.component';
import { EnviarComponent } from './enviar/enviar.component';
import { EnviadasComponent } from './enviadas/enviadas.component';
import { RecibidasComponent } from './recibidas/recibidas.component';


@NgModule({
  declarations: [
    SeleccionarComponent,
    EnviarComponent,
    EnviadasComponent,
    RecibidasComponent
  ],
  imports: [
    CommonModule,
    ModuloSolicitudesRoutingModule,
    ReactiveFormsModule
  ]
})
export class ModuloSolicitudesModule { }
