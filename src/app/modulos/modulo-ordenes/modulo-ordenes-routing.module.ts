import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EnviarComponent } from './enviar/enviar.component';
import { EnviadasComponent } from './enviadas/enviadas.component';
import { RecibidasComponent } from './recibidas/recibidas.component';

const routes: Routes = [
  { path: 'enviar', component: EnviarComponent },
  { path: 'enviadas', component: EnviadasComponent },
  { path: 'recibidas', component: RecibidasComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuloOrdenesRoutingModule { }
