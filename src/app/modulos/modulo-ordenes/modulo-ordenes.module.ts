import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModuloOrdenesRoutingModule } from './modulo-ordenes-routing.module';
import { EnviarComponent } from './enviar/enviar.component';
import { EnviadasComponent } from './enviadas/enviadas.component';
import { RecibidasComponent } from './recibidas/recibidas.component';


@NgModule({
  declarations: [
    EnviarComponent,
    EnviadasComponent,
    RecibidasComponent
  ],
  imports: [
    CommonModule,
    ModuloOrdenesRoutingModule
  ]
})
export class ModuloOrdenesModule { }
