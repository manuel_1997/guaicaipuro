
function init_animation(){

    //menu de imagen de usuario
    const menusito = document.querySelector("#img_user");
        menusito.addEventListener("click", event => {
            const menu_user = document.querySelector(".menusito");
            menu_user.classList.toggle("hidden");
        });

    //ocultar y mostra menu del sistema
    const menubutton = document.querySelector("#btn_menu");
        menubutton.addEventListener("click", event => {
            const menu = document.querySelector(".sidebar");
            menu.classList.toggle("-ml-96");
        });
        
     //ocultar y mostra submenu del menu del sistema
    const submenuButton = document.querySelectorAll(".submenu_btn");
        for(let i = 0; i < submenuButton.length; i++){
            submenuButton[i].addEventListener("click", function(){
                const submenu = this.nextElementSibling;
                const height = submenu.scrollHeight;

                if(submenu.classList.contains("desplegar")){
                    submenu.classList.remove("desplegar");
                    submenu.removeAttribute("style");
                }else{
                    submenu.classList.add("desplegar");
                    submenu.style.height = height + "px";
                }
            })
        }
}

